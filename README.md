# OTP1


## Nimi

Inventaario Sovellus


## Visio

### Tavoite

Tavoitteena on luoda yrityksien k�ytt��n varastonhallinta sovellus, joka helpotta yritysten varastotilanteen seuraamista.

### Sovelluksen toimintoja

Sovellukseemme voi list�t� tuotteitta ja muokata tuotteita jotka ovat tietokannassa. 
K�ytt�liittym�st� voi katsoa muutoshistoriaa inventaarioista ja �low inventory warning� kertoo k�ytt�j�lle kun inventorissa on v�hemm�n kuin ennalta m��r�tt� m��r� tuotetta tai tuotteita.

### K�ytettyj� kehitysymp�rist�j�

- JDK 16
- Scene Builder : k�ytt�littym�n rakentaminen.
- Maven : Projektin ja lis�osien hallinta.
- Jenkins: Jatkuva integrointi.
- Educloud: Et�palvelin.
- MariaDB: Tietokanta.
- JUnit & JaCoCo: Testit.
- Hibernate: Tietokannan k�sittelyyn.

## Asennus & Konfigurointiohjeet

- Asenna JDK 16.
- Varmista, ett� olet kiinni metropolian VPN:ss�.