package com.example.otp1;

import Data.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/***
 * Kommentti
 * @author Leo Lehti�
 * @version
 */

public class MavenTest {


    @Test
    public void tuotelisaystesti(){
        Tuote tuote = new Tuote(); //luodaan tuote
        tuote.setMaara(30); //annetaan tuotteelle tuotteen m��r�
        tuote.muutaMaaraa(15); //muutetaan tuotteen m��r��
        assertEquals(45, tuote.getMaara(), "Tuotteen maaran lisays ei toimi"); //testataan muuttuuku tuotteen m��r�
    }

    @Test
    public void tuotePoistotesti(){
        Tuote tuote = new Tuote(); //luodaan uusi tuote
        tuote.setMaara(30); //annetaan tuotteelle tuotteen m��r�
        tuote.muutaMaaraa(-35); //muutetaan tuotteen m��r��
        assertEquals(0, tuote.getMaara(), "Tuotteen maaran poisto ei toimi"); //testataan meneek� tuote alle nollan
    }

    @Test
    public void valmistajaNimiLisaysTesti(){
        Valmistaja valmistaja = new Valmistaja(); //luodaan valmistaja
        valmistaja.setValmistajaNimi("Valio"); //annetaan valmistajalle nimi
        assertEquals("Valio", valmistaja.getValmistajaNimi(), "Valmistajan nimi ei toimi"); //testataan onnistuiko valmistajalle nimen antaminen
    }


    @Test
    public void valmistajaIDTesti(){
        Valmistaja valmistaja = new Valmistaja(); //luodaan valmistaja
        valmistaja.setValmistajaID(15923); //annetaan valmistajalle ID
        assertEquals(15923, valmistaja.getValmistajaID(), "Valmistajan id ei toimi"); //testataan toimiiko valmistajan ID
    }

    @Test
    public void valmistajaNimijaIDLisaysTesti(){
        Valmistaja valmistaja = new Valmistaja(); //luodaan valmistaja
        valmistaja.setValmistajaNimi("Valio"); //annetaan valmistajalle nimi
        valmistaja.setValmistajaID(123); //annetaan valmistajalle id
        assertEquals("Valio", valmistaja.getValmistajaNimi(), "Valmistajan nimi ei toimi"); //testataan saako valmistaja nimen
        assertEquals(123, valmistaja.getValmistajaID(), "Valmistajan id ei toimi"); //testataan saako valmistaja IDn
    }

    @Test
    public void tyontekijaTesti(){
        Tyontekija tyontekija = new Tyontekija(); //luodaan ty�ntekij�
        tyontekija.setEtunimi("Ville"); //annetaan ty�ntekij�lle etunimi
        tyontekija.setSukunimi("Vallaton"); //annetaan ty�ntekij�lle sukunimi
        tyontekija.setTyontekijaId(9999); //annetaan ty�ntekij�lle id
        tyontekija.setStatus(1); //annetaan ty�ntekij�lle status eli mitk� oikeudet ty�ntekij�ll� on
        tyontekija.setPass("kahvi"); //laitetaan ty�ntekij�lle salasana
        assertEquals("Ville", tyontekija.getEtunimi(), "tyontekijan nimi ei toimi"); //testataan toimiiko etunimi
        assertEquals("Vallaton", tyontekija.getSukunimi(), "tyontekija sukunimi ei toimi"); //testataan toimiiko sukunimi
        assertEquals(9999, tyontekija.getTyontekijaId(), "tyontekija ID ei toimi"); //testataan toimiiko id
        assertEquals(1, tyontekija.getStatus(), "tyontekija status ei toimi"); //testataan toimiiko status
        assertEquals("kahvi", tyontekija.getPass(), "tyontekija salasana ei toimi"); //testataan toimiiko salasana
    }

    @Test
    public void ALVTesti(){
        ALV alv = new ALV(); //luodaan uusi alv
        alv.setAlvID(1); //annetaan alville id
        alv.setAlvprosentti(24); //annetaan alville alvprosentti
        assertEquals(1, alv.getAlvID(), "ALV id ei toimi"); //testataan toimiiko alvin id
        assertEquals(24, alv.getAlvprosentti(), "ALV % ei toimi"); //testataan toimiiko alvin prosentti
    }

    @Test
    public void tuoteTesti(){
        Tuote tuote = new Tuote(); //luodaan uusi tuote
        tuote.setTuoteNimi("Maito"); //annetaan tuotteelle nimi
        tuote.setTuoteKoodi(12345); //annetaan tuotteelle tuotekoodi
        tuote.setHinta(23.56); //annetaan tuotteelle hinta
        tuote.setVaroitusRaja(200); //annetaan tuotteelle varoitusraja
        tuote.setMaara(500); //annetaan tuotteelle tuotteen m��r�
        assertEquals("Maito", tuote.getTuoteNimi(), "tuotenimi ei toimi"); //testaan toimiiko tuotteen nimi
        assertEquals(12345, tuote.getTuoteKoodi(), "tuotekoodi ei toimi"); //testataan toimiiko tuotteen koodi
        assertEquals(23.56, tuote.getHinta(), "tuotehinta ei toimi"); //testataan toimiiko tuotteen hinta
        assertEquals(200, tuote.getVaroitusRaja(), "tuote varoitusraja ei toimi"); //testataan toimiiko varoitusraja
        assertEquals(500, tuote.getMaara(), "tuotem��r� ei toimi"); //testataan toimiiko tuotteen m��r�
    }

    //luodaan uusi muutos muutoshistoriaan, annetaan muutetulle tuotteelle m��r�, id ja kommentti ja testataan toimiiko
    @Test
    public void muutosHistoriaTesti(){
        Muutoshistoria muutos = new Muutoshistoria();
        muutos.setMuutosMaara(50);
        muutos.setMuutosID(12345);
        muutos.setKommentti("Testi");
        assertEquals(50, muutos.getMuutosMaara(), "Muutosm��r� ei toimi");
        assertEquals(12345, muutos.getMuutosID(), "MuutosID ei toimi");
        assertEquals("Testi", muutos.getKommentti(), "Muutoskommentti ei toimi");
    }
}
