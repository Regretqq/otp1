package Data;

import org.hibernate.annotations.GenericGenerator;


import javax.persistence.*;

/***
 * Luokka valmistajien käsittelemiseksi
 * @author Leo Lehtiö ja Felix Uimonen
 * @version 1.0
 */
@Entity
@Table(name ="Valmistaja")
public class Valmistaja {

    /***
     * Valmistajan id tietokantaa varten
     */
    private int valmistajaId;
    /***
     * Valmistajan nimi
     */
    private String valmistajaNimi;

    public Valmistaja(int valmistajaId, String valmistajaNimi){
        this.valmistajaId = valmistajaId;
        this.valmistajaNimi = valmistajaNimi;
    }

    public Valmistaja(){}


    public String getValmistajaNimi() {
        return valmistajaNimi;
    }
    @Column(name="Valmistajanimi")
    public void setValmistajaNimi(String valmistajaNimi) {
        this.valmistajaNimi = valmistajaNimi;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @GenericGenerator(name = "native", strategy = "native")
    public int getValmistajaID() {
        return valmistajaId;
    }

    public void setValmistajaID(int valmistajaId) {
        this.valmistajaId = valmistajaId;
    }
}
