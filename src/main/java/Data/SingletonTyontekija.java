package Data;

/***
 * Sisään kirjautuneen työntekijän käyttäjätietoja hallinoiva luokka
 * @author Joni Tahvanainen
 * @version 1.0
 *
 */
public class SingletonTyontekija {
    /***
     * Luokan instanssi singleton toteutusta varten
     */
    private static SingletonTyontekija instance;
    /***
     * Sisäänkirjautunut työntekijä
     */
    private Tyontekija tyontekija;

    /***
     * Asettaa kirjautuneen työntekijän aktiiviseksi työntekijäksi
     * @param tyontekija Sisäänkirjautunut työntekijä
     */
    private SingletonTyontekija(Tyontekija tyontekija){
        this.tyontekija = tyontekija;
    }

    /***
     * Asettaa kirjautuneen työntekijän aktiiviseksi työntekijäksi ja luo instanssin tarvittesssa
     * @param tyontekija Sisäänkirjautunut työntekijä
     * @return palauttaa päivitetyn instanssin
     */
    public static SingletonTyontekija setIntance(Tyontekija tyontekija){
        if(instance == null){
            instance = new SingletonTyontekija(tyontekija);
        }else{
            instance.tyontekija = tyontekija;
        }
        return instance;
    }

    /***
     * Palauttaa singleton instanssin
     * @return singletonin instansssi
     */
    public static SingletonTyontekija getInstance(){
        return instance;
    }

    /***
     * palauttaa aktiivisen työntekijän
     * @return Työntekijä
     */
    public Tyontekija getTyontekija(){
        return tyontekija;
    }


}
