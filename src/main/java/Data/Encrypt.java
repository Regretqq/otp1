package Data;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/***
 * Salasanojen encryptaamis luokka.
 * @author Joni Tahvanainen
 * @version 1
 *
 */
public class Encrypt {
    /**
     * On salattu Salasana.
     */
    private static String encryptedpassword;

    /**
     * Otamme salasanan parametrina funcktioon ja encryptaamme tämän.
     * @param salasana String joka halutaan encryptaa.
     * @return encryptedpassword Annettu String encryptattu.
     */
    public static String encrypt(String salasana){
        encryptedpassword = null;
        try
        {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(salasana.getBytes());
            byte[] bytes = m.digest();
            StringBuilder s = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                s.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }

            encryptedpassword = s.toString();
            return encryptedpassword;

        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
