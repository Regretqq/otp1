

package Data;

import javax.persistence.*;


/***
 * Luokka Tuotteiden käsittellyyn
 * @author Leo Lehtiö ja Felix Uimonen
 * @version 1.0
 */
@Entity
@Table(name="Tuotteet")
public class Tuote {

    /***
     * Tuotteen id
     */
    private int tuoteKoodi;
    /***
     * Tuotteen nimi
     */
    private String tuoteNimi;
    /***
     * Tuoteen kappalemäärä
     */
    private int maara;
    /***
     * Raja tuottemäärän vähäisyyden määrittelemiseksi
     */
    private int varoitusRaja;
    /***
     * Tuotteen hinta
     */
    private double hinta;
    /***
     * Tuotteen valmistaja
     */
    private Valmistaja valmistaja;
    /***
     * Tuotteen alv
     */
    private ALV alv;

    public Tuote(){}

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ValmistajaID")
    public Valmistaja getValmistajaId() {
        return valmistaja;
    }
    public void setValmistajaId(Valmistaja valmistaja) {
        this.valmistaja = valmistaja;
    }

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "AlvID")
    public ALV getAlv() {
        return alv;
    }
    public void setAlv(ALV alv) {
        this.alv = alv;
    }

    @Column(name = "Hinta")
    public double getHinta() {
        return hinta;
    }
    public void setHinta(double hinta) {
        this.hinta = hinta;
    }

    @Column(name = "Varoitusraja")
    public int getVaroitusRaja() {
        return varoitusRaja;
    }
    public void setVaroitusRaja(int varoitusRaja) {
        this.varoitusRaja = varoitusRaja;
    }

    @Column(name = "maara")
    public int getMaara() {
        return maara;
    }
    public void setMaara(int maara) {
        this.maara = maara;
    }

    @Column(name = "Tuotenimi")
    public String getTuoteNimi() {
        return tuoteNimi;
    }
    public void setTuoteNimi(String tuoteNimi) {
        this.tuoteNimi = tuoteNimi;
    }

    @Id
    @Column(name ="Tuotekoodi")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getTuoteKoodi() {
        return tuoteKoodi;
    }
    public void setTuoteKoodi(int tuoteKoodi) {
        this.tuoteKoodi = tuoteKoodi;
    }

    /***
     * Tuotteen määrän muutttamiseen käytettävä metodi
     * @param muutos Muutoksen määrä
     */
    public void muutaMaaraa(int muutos){
        maara += muutos;
        if(maara < 0){
            maara = 0;
        }
    }
}




