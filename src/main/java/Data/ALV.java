package Data;

import javax.persistence.*;

/***
 * Luokka alv:en käsittelyyn
 * @author Leo Lehtiö ja Felix Uimonen
 */
@Entity
@Table(name = "Alvprosentit")
public class ALV {
    /***
     * ALV id tietokantaa varten
     */
    private int AlvID;
    /***
     * Alvprosentti
     */
    private float Alvprosentti;

    @Id
    @Column(name ="AlvID")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getAlvID() {
        return AlvID;
    }

    public void setAlvID(int alvID) {
        AlvID = alvID;
    }
    @Column(name = "Alvprosentti")
    public float getAlvprosentti() {
        return Alvprosentti;
    }

    public void setAlvprosentti(float alvprosentti) {
        Alvprosentti = alvprosentti;
    }
}
