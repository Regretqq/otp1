package Data;

import javax.persistence.*;

/***
 * Luokka työntekijöiden käsittelyyn
 * @author Leo Lehtiö ja Felix Uimonen
 * @version 1.0
 */
@Entity
@Table(name = "Tyontekija")
public class Tyontekija {
    /***
     * Työntekijän id tietokantaa varten
     */
    private int tyontekijaId;
    /***
     * Työntekijän rooli(esimies, työntekijä jne.)
     */
    private int status;
    /***
     * Työntekijän etunimi
     */
    private String etunimi;
    /***
     * Työntekijän sukunimi
     */
    private String sukunimi;
    /***
     * Työntekijän salasana järjestelmään
     */
    private String pass;


    public Tyontekija(){}

    public void setPass(String pass){ this.pass = pass;}
    @Column(name = "Pass")
    public String getPass () {return pass;}

    @Column(name = "Sukunimi")
    public String getSukunimi() {
        return sukunimi;
    }

    public void setSukunimi(String sukunimi) {
        this.sukunimi = sukunimi;
    }
    @Column(name = "Etunimi")
    public String getEtunimi() {
        return etunimi;
    }

    public void setEtunimi(String etunimi) {
        this.etunimi = etunimi;
    }
    @Column(name = "Status")
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    @Id
    @Column(name = "TyontekijaID")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getTyontekijaId() {
        return tyontekijaId;
    }

    public void setTyontekijaId(int tyontekijaId) {
        this.tyontekijaId = tyontekijaId;
    }

    /***
     * Kertoo työntekijän roolin valitulla kielellä
     * @return Työntekijän rooli tekstinä
     */
    public String status() {
        switch (status){
            case 1:
                return "Esimies";
            case 2:
                return "Tyontekija";
            default:
                return "Ei Aktiivinen";
        }
    }
}
