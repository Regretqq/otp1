package Data;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/***
 * Hibernate apuluokka, joka luo yhteyden tietokantaan hibernate.cfg.xml tiodoston avulla ja ylläpitää sitä
 * @author Felix Uimonen
 * @version 1.3
 */
public class HibernateUtil {
    /***
     * Luokan intanssi singleton toteutusta varten
     */
    private static HibernateUtil instance = null;
    /***
     * Tietokanta yhteyden hallinassa käytetävä Luokka
     * @see SessionFactory
     */
    private static SessionFactory factory;

    /***
     * Luo yhteyden tietokantaan
     */
    private HibernateUtil(){
        try {
            factory = new Configuration().configure().buildSessionFactory();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Istuntotehtaan luominen ei 	onnistunut.");
            System.exit(-1);
        }
    }

    /***
     * Luo tarvittaessa HibernateUtil instanssin palauttaa sen
     * @return HibernateUtil instanssi
     */
    public static HibernateUtil getHibernateUtil(){
        if (instance == null){
            instance = new HibernateUtil();
        }
        return instance;
    }

    /***
     * Luo tarvittaessa HibernateUtil instanssin
     * @return Sessionfactory
     */
    public static SessionFactory getSessionFactory(){
        if (instance == null){
            instance = new HibernateUtil();
        }
        return factory;
    }

    /***
     * Tietkantaan talentamiseen käytettävä metodi
     * @param o tietokantaan talenttava olio
     */
    public void save(Object o){
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        session.saveOrUpdate(o);
        tx.commit();
        session.close();
    }

}
