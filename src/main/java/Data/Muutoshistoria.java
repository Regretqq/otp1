package Data;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;

/***
 * Tietokantaan tehtyjen muutosten pohjalta syntyvä lokitieto, joka paljastaa muutoksen tehneen työntekijä, muutoksen määrän, sen ajankohdan jatuotteen johon muutos kohdistuu
 * @author Leo Lehtiö ja Felix Uimonen
 * @version 1.0
 */
@Entity
@Table(name = "MuutosHistoria")
public class Muutoshistoria {
    /***
     * Muutoksen päivämäärä
     */
    private LocalDate paivaMaara;
    /***
     * Muutoksen suuruus
     */
    private int muutosMaara;
    /***
     * Muutoksen suunta
     */
    private int inAndOut;
    /***
     * Muutoksen id tietokantaa varten
     */
    private int MuutosID;
    /***
     * Muutokseen liittyyvä komentti muutoksen tekijältä
     */
    private String kommentti;
    /***
     * tuote jota muutos koskee
     */
    private Tuote tuote;
    /***
     * Muutoksen tehnyt työntekijä
     */
    private Tyontekija tyontekija;

    public Muutoshistoria(){}


    @Id
    @Column(name = "MuutosID")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getMuutosID(){
        return MuutosID;
    }

    public void setMuutosID(int MuutosID){
        this.MuutosID = MuutosID;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "TyontekijaID")
    public Tyontekija getTyontekija() {
        return tyontekija;
    }

    public void setTyontekija(Tyontekija tyontekija) {
        this.tyontekija = tyontekija;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "Tuotekoodi")
    public Tuote getTuote() {
        return tuote;
    }

    public void setTuote(Tuote tuote) {
        this.tuote = tuote;
    }
    @Column(name = "Komentti")
    public String getKommentti() {
        return kommentti;
    }

    public void setKommentti(String kommentti) {
        this.kommentti = kommentti;
    }

    @Column(name = "SisaanUlos")
    public int getInAndOut() {
        return inAndOut;
    }

    public void setInAndOut(int inAndOut) {
        this.inAndOut = inAndOut;
    }

    @Column(name = "Muutosmaara")
    public int getMuutosMaara() {
        return muutosMaara;
    }

    public void setMuutosMaara(int muutosMaara) {
        this.muutosMaara = muutosMaara;
    }

    @Column(name = "paivamaara")
    public LocalDate getPaivaMaara() {
        return paivaMaara;
    }

    public void setPaivaMaara(LocalDate paivaMaara) {
        this.paivaMaara = paivaMaara;
    }
}
