package Data;

import java.io.FileInputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

/***
 * Ohjelman lokalisoinissa käytettävä luokka, joka käsitellee resourcebundelejä sekä localeja
 * @author Felix Uimonen
 * @version 1.0
 */
public class Bundle {

    /***
     * Käytössä oleva lokaali
     */
    private static Locale curLocale;
    /***
     * Käytössä oleva resourcebundle
     */
    private static ResourceBundle bundle;
    /***
     * Luokan instanssi singleton toteutusta varten
     */
    private static Bundle instance = null;

    /***
     * Lataa oletus lokaalin
     */
    private Bundle(){
        String configPath = "resources/Language.properties";
        Properties properties = new Properties();
        try{
            properties.load(new FileInputStream(configPath));
            String language = properties.getProperty("language");
            String country = properties.getProperty("country");
            curLocale = new Locale(language, country);
            Locale.setDefault(curLocale);
        }catch (Exception e){
            curLocale = new Locale("fi", "FI");
            Locale.setDefault(curLocale);
        }
    }

    /***
     * Luo instanssin tarvittaessa ja hakee lokaalin mukaisen resource bundlen
     * @return Luokan instanssin
     */
    public static Bundle getBundle(){
        if(bundle == null){
            instance = new Bundle();
            bundle = ResourceBundle.getBundle("ResourceBundle.testi", curLocale);
        }
        return instance;
    }

    /***
     * palautta käytössä olevan resourcebundlen
     * @return Resourcebundle
     */
    public ResourceBundle getResourceBundle(){
        return bundle;
    }

    /***
     * Asettaa uuden lokaalin ja vaihtaa resourcebundlen uuden lokaalin mukaiseksi
     * @param language lokaalin kielikoodi
     * @param country lokaalin maakoodi
     */
    public void setLocale(String language, String country){
        curLocale = new Locale(language, country);
        Locale.setDefault(curLocale);
        bundle = ResourceBundle.getBundle("ResourceBundle.testi", curLocale);
    }


}
