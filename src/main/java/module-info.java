module com.example.otp1 {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires javafx.graphics;
    requires java.sql;
    requires java.persistence;
    requires org.hibernate.orm.core;
    requires java.naming;
    requires java.sql.rowset;


    opens com.example.otp1 to javafx.fxml;
    exports com.example.otp1;
    exports Data;
    opens Data to javafx.fxml, org.hibernate.orm.core;


}