package com.example.otp1;

import Data.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.text.DateFormat;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.ResourceBundle;
/***
 * Historia ikkunan ohjain. Näkymästä voi tarakastella tietokantaan lisätyöstä tuotteista lisätietoa:
 * Esimerkiksi milloin tuote oli lisätty ja kuka työntekijä oli joka lisäsi tuotetta.
 * @author Felix Uimonen
 * @version 1
 *
 */
public class HistoriaController {
    /**
     * Lista Työntekijöistä
     */
    private List<Tyontekija> työntekijäLista;
    /**
     * Resurssipaketti
     */
    private ResourceBundle bundle;
    /**
     * Kommnetti muokkas boxin muuttuja
     */
    private boolean muokkaa = false;

    /**
     * Muokkaa nappi.
     */
    @FXML
    private Button muokkaaButton;
    /**
     * Hakukenttä.
     */
    @FXML
    private TextField hakukenttä;
    /**
     * Historia taulu
     */
    @FXML
    private TableView historiatable;
    /**
     * Kommentti tekstikenttä.
     */
    @FXML
    private TextArea komentti;
    /**
     * Työntekijä valinta laatikko.
     */
    @FXML
    private ChoiceBox työntekijäBox;
    /**
     * Alkupäivämäärä
     */
    @FXML
    private DatePicker alkuPVM;
    /**
     * Loppupäivämäärä.
     */
    @FXML
    private DatePicker loppuPVM;
    /**
     * Taulurivi tuoteelle.
     */
    @FXML
    private TableColumn<Muutoshistoria, String> tuote;
    /**
     * Taulurivi muutosid
     */
    @FXML
    private TableColumn<Muutoshistoria, String> muutosid;
    /**
     * Taulurivi muutos määrälle.
     */
    @FXML
    private TableColumn<Muutoshistoria, String> muutos;
    /**
     * Muutos määrän suunta joko + tai -
     */
    @FXML
    private TableColumn<Muutoshistoria, String> suunta;
    /**
     * Taulurivi työntekijästä joka muutti tuotetta.
     *
     */
    @FXML
    private TableColumn<Muutoshistoria, String> työntekijä;
    /**
     * Taulurivi päivämäärästä jolloin muutos tehtiin.
     */
    @FXML
    private TableColumn<Muutoshistoria, String> päivämäärä;

    /**
     * Valmistelee historia ikkunan lokalisoinnin oikeaksi ja hakee tiedot tauluun hibernaten avulla.
     */
    @FXML
    private void initialize(){
        bundle = Bundle.getBundle().getResourceBundle();
        tuote.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTuote().getTuoteNimi()));
        muutosid.setCellValueFactory(new PropertyValueFactory<>("MuutosID"));
        muutos.setCellValueFactory(new PropertyValueFactory<>("muutosMaara"));
        suunta.setCellValueFactory(cellData -> new SimpleStringProperty(suunta(cellData.getValue().getInAndOut())));
        työntekijä.setCellValueFactory(cellData -> new SimpleStringProperty(
                            cellData.getValue().getTyontekija().getEtunimi() + " " +
                                    cellData.getValue().getTyontekija().getSukunimi()));
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM);
        päivämäärä.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPaivaMaara().format(formatter)));
        getHistoria();
        getTyöntekijäBox();



    }

    /**
     * Hakee historia tauluun tarvittavat tiedot.
     */
    private void getHistoria(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("From Data.Muutoshistoria");
        List<Muutoshistoria> lista = query.list();
        historiatable.getItems().clear();
        historiatable.getItems().addAll(lista);
    }

    /**
     * Hakukentän hakusanalla etsiminen historia taulusta.
     * @param haku hakusana.
     */
    private void getHakuHistoria(String haku){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query;
        if(työntekijäBox.getSelectionModel().getSelectedIndex() == 0 ) {
            if(alkuPVM.getValue() == null && loppuPVM.getValue() == null){
            query = session.createQuery("From Data.Muutoshistoria a where a.tuote.tuoteNimi like '%" + haku + "%'");
            }else if(alkuPVM.getValue() == null){
                query = session.createQuery("From Data.Muutoshistoria a where a.tuote.tuoteNimi like '%" + haku + "%'" +
                        "and a.paivaMaara <= '" + loppuPVM.getValue() + "'");
            }
            else if(loppuPVM.getValue() == null){
                query = session.createQuery("From Data.Muutoshistoria a where a.tuote.tuoteNimi like '%" + haku + "%'" +
                        "and a.paivaMaara >= '" + alkuPVM.getValue() + "'");
            }
            else{
                query = session.createQuery("From Data.Muutoshistoria a where a.tuote.tuoteNimi like '%" + haku + "%'" +
                        "and a.paivaMaara >= '" + alkuPVM.getValue() + "' and a.paivaMaara <= '" + loppuPVM.getValue() + "'");
            }

        }else{
            int index = työntekijäBox.getSelectionModel().getSelectedIndex() - 1;
            if(alkuPVM.getValue() == null && loppuPVM.getValue() == null) {
                query = session.createQuery("From Data.Muutoshistoria a where a.tuote.tuoteNimi like '%" + haku + "%' " +
                        "and a.tyontekija.tyontekijaId = " + työntekijäLista.get(index).getTyontekijaId());
            }else if(alkuPVM.getValue() == null){
                query = session.createQuery("From Data.Muutoshistoria a where a.tuote.tuoteNimi like '%" + haku + "%' " +
                        "and a.tyontekija.tyontekijaId = " + työntekijäLista.get(index).getTyontekijaId() +
                        " and  a.paivaMaara <= '" + loppuPVM.getValue() + "'");
            }else if(loppuPVM.getValue() == null){
                query = session.createQuery("From Data.Muutoshistoria a where a.tuote.tuoteNimi like '%" + haku + "%' " +
                        "and a.tyontekija.tyontekijaId = " + työntekijäLista.get(index).getTyontekijaId() +
                        " and  a.paivaMaara >= '" + alkuPVM.getValue() + "'");
            }else {
                query = session.createQuery("From Data.Muutoshistoria a where a.tuote.tuoteNimi like '%" + haku + "%' " +
                        "and a.tyontekija.tyontekijaId = " + työntekijäLista.get(index).getTyontekijaId() +
                        " and  a.paivaMaara >= '" + alkuPVM.getValue() + "' and a.paivaMaara <= '" + loppuPVM.getValue() + "'");
            }
        }
        List<Muutoshistoria> lista = query.list();
        historiatable.getItems().clear();
        historiatable.getItems().addAll(lista);
    }

    /**
     * Palauttaa parametrin perusteella suunta tekstin resurssipaketin perusteella.
     * @param i suunta id
     * @return palauttaa tekstin resurssipaketin perusteella
     */
    private String suunta(int i){
        if(i == 0) {
            return bundle.getString("sisään.text");
        }else {
            return bundle.getString("ulos.text");
        }
    }

    /**
     * Historiataulusta valittu muutos näkyviin funktio.
     */
    @FXML
    private void onHistoriaTableAction(){
        muokkaa = false;
        komentti.editableProperty().set(false);
        muokkaaButton.setText(bundle.getString("muokkaaButton.text"));
        Muutoshistoria historia = (Muutoshistoria) historiatable.getSelectionModel().getSelectedItem();
        if(historia != null) {
            komentti.clear();
            komentti.setText(historia.getKommentti());
        }

    }

    /**
     * Haku napin funktio.
     */
    @FXML
    private void onHakuButtonAction(){
        String haku = hakukenttä.getText();
        getHakuHistoria(haku);
    }

    /**
     * Hakee työntekijät hibernaten avulla tietokannasta ja lisää ne työntekijä valinta ruutuun.
     */
    private void getTyöntekijäBox(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from Data.Tyontekija");
        työntekijäLista = query.list();
        työntekijäBox.getItems().add(bundle.getString("kaikki.text"));
        työntekijäBox.getSelectionModel().selectFirst();
        for(int i = 0; i < työntekijäLista.size(); i++){
            työntekijäBox.getItems().add(työntekijäLista.get(i).getEtunimi() + " " + työntekijäLista.get(i).getSukunimi());
        }

    }

    /**
     * Muokkaa napin funktio. Joka antaa mahdollisuuden muokata muutoshistoriassa olevia kommentteja.
     */
   @FXML
   private void onMuokkabuttonAction(){
        if(muokkaa){

            Muutoshistoria historia = (Muutoshistoria) historiatable.getSelectionModel().getSelectedItem();
            historia.setKommentti(komentti.getText());
            HibernateUtil.getHibernateUtil().save(historia);
            komentti.editableProperty().set(false);
            muokkaa = false;
            muokkaaButton.setText(bundle.getString("Muokkaa.text"));
        }else{
            muokkaaButton.setText(bundle.getString("Tallenna.text"));
            komentti.editableProperty().set(true);
            muokkaa = true;
        }
   }

}
