package com.example.otp1;

import Data.Bundle;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;


import java.io.IOException;
import java.util.ResourceBundle;
/***
 * Hoitaa sovelluksen avaamisen ja avaa ensimmäisenä kirjautumis ruudun.
 * @author Joni Tahvanainen
 * @version 1
 *
 */
public class MainApplication extends Application {
    /**
     * JavaFx Stage
     */
    Stage stage;
    /**
     * Resurssipaketti
     */
    ResourceBundle bundle;

    /**
     * haetaan resurssipaketti
     * luodaan fxmlLoader joka hakee mainApplication luokasta login-view resurssin.
     * fxmlloaderille annetaan resurssipaketti resurssiksi
     * luodaan uusi skene joka on login-view
     * skene laitetaan päälle ja sovellus käynnistyy
     * @param stage JavaFX Stage
     * @throws IOException Error viesti
     */
    @Override
    public void start(Stage stage) throws IOException {
        bundle = Bundle.getBundle().getResourceBundle();
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("login-view.fxml"));
        fxmlLoader.setResources(bundle);
        Scene scene = new Scene(fxmlLoader.load());
        LoginView controller = fxmlLoader.getController();
        controller.initData(this);
        stage.setTitle(bundle.getString("mainTitle.text"));
        stage.setScene(scene);
        stage.show();
        this.stage = stage;


    }

    /**
     * haetaan resurssipaketti
     * luodaan fxmlLoader joka hakee mainApplication luokasta main view resurssin
     * laitetaan fxmlLoaderille resurssipaketti resursseksi
     * luodaan uusi skene joka on main view
     * näkymä vaihtuu login viewistä main viewiin kun kirjaudutaan sisään
     * @throws IOException Error viesti
     */

    public void update() throws IOException {
        bundle = Bundle.getBundle().getResourceBundle();
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("main-view.fxml"));
        fxmlLoader.setResources(bundle);
        Scene scene = new Scene(fxmlLoader.load());
        MainController controller = fxmlLoader.getController();
        controller.initData(this);
        stage.setTitle(bundle.getString("mainTitle.text"));
        stage.setScene(scene);
        stage.show();
    }

    /**
     * käynnistää sovelluksen
     * @param args java args
     */
    public static void main(String[] args) {
        launch();
    }
}