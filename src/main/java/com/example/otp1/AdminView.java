package com.example.otp1;

import Data.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

/***
 * Ohjauspaneelin ohjain. Jossa voit lisätä uusia käyttäjiä sovellukseen tai muokata olemassa olevia käyttäjien tietoja.
 *
 * @author Joni Tahvanainen
 * @version 2.5
 */
public class AdminView {
    /**
     * Resurssipaketti lokalisointia varten.
     */
    private ResourceBundle bundle;
    /**
     * Etunimi tekstikenttä
     */
    @FXML
    private TextField etunimi;
    /**
     * Sukunimi tekstikenttä
     */
    @FXML
    private TextField sukunimi;
    /**
     * Salasanan tekstikenttä
     */
    @FXML
    private TextField salasana;
    /**
     * Työntekijän tilan valinta laatikko
     */
    @FXML
    private ComboBox statusbox;
    /**
     * Haku kenttä.
     */
    @FXML
    private TextField hakuKenttä;
    /**
     * Valitun käyttäjän etunimi
     */
    @FXML
    private TextField selectetunimi;
    /**
     * Valitun käyttäjän sukunimi
     */
    @FXML
    private TextField selectsukunimi;
    /**
     * Valitun käyttäjän oikeus tila.
     */
    @FXML
    private ComboBox selectstatus;
    /**
     * Työntekijöiden taulu.
     */
    @FXML
    private TableView tyontekijataulu;
    /**
     * Taulurivi jossa on etunimet
     */
    @FXML
    private TableColumn<Tyontekija, String> etunimitable;
    /**
     * Taulurivi jossa on sukunimet
     */
    @FXML
    private TableColumn<Tyontekija, String> sukunimitable;
    /**
     * Taulurivi jossa on työntekijän oikeustila.
     */
    @FXML
    private TableColumn<Tyontekija, String> status;
    /**
     * Peruuta nappi
     */
    @FXML
    private Button peruutabutton;
    /**
     * Tallenna nappi
     */
    @FXML
    private Button tallennabutton;
    /**
     * Valitun työntekijän salasana.
     */
    private String selectpass;

    /**
     * Työntekijöiden lista.
     */
    private List<Tyontekija> työntekijälista;

    /**
     * Valmistellaan Adminview käyttökelpoiseks.
     * Otetaan oikea resurssipaketti käyttöön localisointia varten ja haetaan tarvittavat tiedot TableViewhin tietokannasta.
     */
    @FXML
    private void initialize(){
        bundle = Bundle.getBundle().getResourceBundle();
        etunimitable.setCellValueFactory(new PropertyValueFactory<>("etunimi"));
        sukunimitable.setCellValueFactory(new PropertyValueFactory<>("sukunimi"));
        status.setCellValueFactory(cellData -> new SimpleStringProperty((cellData.getValue().status())));
        setTyontekijalista(getTyontekijaLista());
        selectstatus.getItems().addAll("Admin", "Työntekija", "Ei aktiivinen");
        statusbox.getItems().addAll("Admin", "Työntekija", "Ei aktiivinen");
        statusbox.getSelectionModel().select(1);
        bundle = Bundle.getBundle().getResourceBundle();
        tallennabutton.setDisable(true);
    }

    /**
     * Getteri jolla haetaan hibernaten avulla kaikki tiedot työntekijöistä.
     * @return lista työntekijöitä.
     */
    private List<Tyontekija> getTyontekijaLista() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query tuoteQuery = session.createQuery("From Data.Tyontekija");
        List<Tyontekija> lista = tuoteQuery.list();
        return(lista);

    }

    /**
     * Hakukentän funktio. Hakukenttään pistetään tarvittavat.
     * @param haku parametri siitä mitä haluataan hakea Tableview:sta.
     * @return Palauttaa listan jossa on vain ainoastaan hakukenttään sopivat asiat.
     */
    private List<Tyontekija> haeTyontekijaLista(String haku){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query tuoteQuery = session.createQuery("From Data.Tyontekija a where a.etunimi like '%" + haku +"%' or a.sukunimi like '%" + haku + "%' ");
        List<Tyontekija> lista = tuoteQuery.list();
        return lista;

    }

    /**
     * Lisää listan työntekijöitä näkyviin TableView:hin.
     * @param lista työntekijä lista.
     */
    private void setTyontekijalista(List<Tyontekija> lista){
        tyontekijataulu.getItems().clear();
        tyontekijataulu.getItems().addAll(lista);
    }

    /**
     * Funktio uuden työntekijä lisäyksestä.
     * Funktio käy kaikki tarvittavat tekstikentät läpi ja tarkistaa nämä ovatko ne kunnossa. Jos nämä ovat kunnossa lisää funktio uuden työntekijän hibernaten avulla tietokantaan.
     * Jos tekstikentät ei ole kunnossa ilmoittaa funktio käyttäjälle alert.error ilmoituksen. Jossa on syy miksi ei funktio mennyt läpi.
     */
    @FXML
    private void handlenew(){

        Tyontekija save = new Tyontekija();
        if(etunimi.getText().length() <= 0 || sukunimi.getText().length() <= 0 || salasana.getText().length() <= 0) {
            createAlert(bundle.getString("AdminVirheellinen.text"));

        } else if (etunimi.getText().length() > 30 || sukunimi.getText().length() > 30 || salasana.getText().length() > 100) {
            createAlert(bundle.getString("AdminVirheellinen.text"));
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(bundle.getString("varmistus.text"));
            alert.setHeaderText(null);
            alert.setContentText(bundle.getString("varma.text"));
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                save.setEtunimi(etunimi.getText());
                save.setSukunimi(sukunimi.getText());
                save.setPass(Encrypt.encrypt(salasana.getText()));
                save.setStatus(statusbox.getSelectionModel().getSelectedIndex()+1);
                HibernateUtil.getHibernateUtil().save(save);
                etunimi.setText("");
                sukunimi.setText("");
                salasana.setText("");
                setTyontekijalista(getTyontekijaLista());

            }

        }
    }

    /**
     * Muokkaa selectetunimi, selectsukunimi, selectstatus ja selectpass näkyviin tekstikenttiin, jotta niitä voi muokata.
     */
    @FXML
    private void handleEdit(){
        if(tyontekijataulu.getSelectionModel().getSelectedItem() != null) {
            selectetunimi.setEditable(true);
            selectsukunimi.setEditable(true);
            tallennabutton.setDisable(false);
            Tyontekija tyontekija = (Tyontekija) tyontekijataulu.getSelectionModel().getSelectedItem();
            selectetunimi.setText(tyontekija.getEtunimi());
            selectsukunimi.setText(tyontekija.getSukunimi());
            selectstatus.getSelectionModel().select(tyontekija.status());
            selectpass = tyontekija.getPass();
        }

    }

    /**
     * Peruuta nappi sulkee ohjauspaneeli ikkunan.
     */
    @FXML
    private void handleperuuta(){
        Stage stage = (Stage) peruutabutton.getScene().getWindow();
        stage.close();
    }
    /**
     * Funktio muokaa olemassa olevaa työntekijä.
     * Funktio käy kaikki tarvittavat tekstikentät läpi ja tarkistaa nämä ovatko ne kunnossa. Jos nämä ovat kunnossa funktio päivittää työntekijän tietoja hibernaten avulla tietokannassa.
     * Jos tekstikentät ei ole kunnossa ilmoittaa funktio käyttäjälle alert.error ilmoituksen. Jossa on syy miksi ei funktio mennyt läpi.
     */
    @FXML
    private void handlenewedit(){
        Tyontekija save = (Tyontekija) tyontekijataulu.getSelectionModel().getSelectedItem();
        if(selectetunimi.getText().length() <= 0 || selectsukunimi.getText().length() <= 0) {
            createAlert(bundle.getString("AdminVirheellinen.text"));

        } else if (selectetunimi.getText().length() > 30 || selectsukunimi.getText().length() > 30) {
            createAlert(bundle.getString("AdminVirheellinen.text"));
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(bundle.getString("varmistus.text"));
            alert.setHeaderText(null);
            alert.setContentText(bundle.getString("toinenvarma.text"));
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                save.setEtunimi(selectetunimi.getText());
                save.setSukunimi(selectsukunimi.getText());
                save.setStatus(selectstatus.getSelectionModel().getSelectedIndex()+1);
                save.setPass(selectpass);
                HibernateUtil.getHibernateUtil().save(save);
                setTyontekijalista(getTyontekijaLista());
                selectetunimi.setEditable(false);
                selectsukunimi.setEditable(false);
                selectsukunimi.setText("");
                selectetunimi.setText("");
                selectstatus.getSelectionModel().clearSelection();
                tallennabutton.setDisable(true);

            }

        }
    }

    /**
     * päivittää työntekijälistan kun hae nappia painetaan.
     * @param event kuuntelee muutoksia hakunapista.
     */
    @FXML
    private void handlehaku(ActionEvent event){
        String haku = hakuKenttä.getText();
        setTyontekijalista(haeTyontekijaLista(haku));
    }

    /**
     * Tuottaa ilmotuuksen käyttäjälle.
     * @param alertText varoitusteksti
     */
    public void createAlert(String alertText){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(null);
        alert.setContentText(alertText);
        alert.showAndWait();
    }
}
