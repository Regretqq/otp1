package com.example.otp1;

import Data.ALV;
import Data.Bundle;
import Data.HibernateUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ResourceBundle;
/***
 * add-ALV ikkunan ohjain.
 * @author Joni Tahvanainen, Leo Lehtiö
 * @version 1.1
 *
 */
public class AddAlvView {

    Alert alert = new Alert(Alert.AlertType.ERROR);
    /**
     * Sulkee Ikkunan
     */
    @FXML
    private Button closeButton;
    /**
     * ALV tekstikenttä.
     */
    @FXML
    private TextField ALVTextField;

    /***
     * ALV:in lisäys funktio.
     * Lisää ALVin hibernaten avulla tietokantaan.
     * Ennen tätä kumminkin tarkistaa ALV onko tämä kelvollinen ALV.
     * Jos ei ole  luodaan alert.
     */
    @FXML
    private void handleAdd(){
        ResourceBundle bundle = Bundle.getBundle().getResourceBundle();
        ALV alv = new ALV();
        if (!ALVTextField.getText().matches("^([0-9]{1,2}){1}(\\.[0-9]{1,2})?$")) {
            alert.setContentText(bundle.getString("AlvEiOleNumeroAlert.text"));
            alert.show();
        } else {
            alv.setAlvprosentti(Float.parseFloat(ALVTextField.getText()));

            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();
            session.save(alv);
            tx.commit();
            session.close();

            Stage stage = (Stage) closeButton.getScene().getWindow();
            stage.close();
        }
    }
}
