package com.example.otp1;

import Data.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.query.Query;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;
/***
 * Koko sovelluksen ydin. Tämä on pääikkunan ohjain. Jossa voi selata inventoorissa olevia tuotteita ja pääsy kaikkiin muihin ikkunoihin
 * Esimerkiksi: Ohjauspaneeliin tai Lisäämään uusi Tuote.
 * @author Joni Tahvanainen, Felix Uimonen
 * @version 1
 *
 */
public class MainController {
    /**
     * Tämä ohjain.
     */
    MainApplication mainApplication;
    /**
     * Resurssipaketti.
     */
    private ResourceBundle bundle;
    /**
     * Tuotekoodilla hakemis kenttä.
     */
    @FXML
    private TextField tuotekoodiHaku;
    /**
     * Sulkemis nappi.
     */
    @FXML
    private Button closeButton;
    /**
     * Tuotteiden taulu.
     */
    @FXML
    private TableView tuotelista;
    /**
     * Hakukenttä.
     */
    @FXML
    private TextField hakuKenttä;
    /**
     * Työntekijän nimi.
     */
    @FXML
    private Text statusText;
    /**
     * Ohjauspaneelin avaus nappi.
     */
    @FXML
    private Button adminbutton;
    /**
     * Valitun tuotteen muokkaus ikkunnan avaus nappi.
     */
    @FXML
    private Button muokkaaButton;
    /**
     * Uuden tuotteen lisäys ikkunan avaus nappi.
     */
    @FXML
    private Button lisaaButton;
    /***
     * Taulurivi tuotteiden nimistä.
     */
    @FXML
    private TableColumn<Tuote, String> Tuotenimi;
    /**
     * Taulurivi tuotteiden koodista.
     */
    @FXML
    private TableColumn<Tuote, String> Tuotekoodi;
    /**
     * Taulurivi tuotteiden määrästä.
     */
    @FXML
    private TableColumn<Tuote, String> Maara;
    /**
     * Taulurivi tuotteiden hinnasta.
     */
    @FXML
    private TableColumn<Tuote, String> Hinta;
    /**
     * Taulurivi tuotteiden valmistajista.
     */
    @FXML
    private TableColumn<Tuote, String> Valmistaja;
    /**
     * Työntekijöiden lista.
     */
    private List<Tyontekija> työntekijälista;
    /**
     * Decimaali formatoija.
     */
    private DecimalFormat df;

    /**
     * Valmistellaan Main ruudun käyttöä varten asettaa taululle tyylit.
     * Hakee kaikki tarvittavat tiedot tietokannasta.
     * Ja jos kirjautunut henkilö ei ole esimies asettaa jotkin elementit piilotetuiksi.
     */
    @FXML
    private void initialize(){
        bundle = Bundle.getBundle().getResourceBundle();
        Hinta.setStyle("-fx-alignment: CENTER-RIGHT;");
        Maara.setStyle("-fx-alignment: CENTER-RIGHT;");
        Tuotekoodi.setStyle("-fx-alignment: CENTER-RIGHT;");
        df = new DecimalFormat("####0.00");
        Tyontekija tyontekija = SingletonTyontekija.getInstance().getTyontekija();
        Tuotenimi.setCellValueFactory(new PropertyValueFactory<>("tuoteNimi"));
        Tuotekoodi.setCellValueFactory(new PropertyValueFactory<>("tuoteKoodi"));
        Maara.setCellValueFactory(new PropertyValueFactory<>("maara"));
        Locale locale = Locale.getDefault();
        if(!locale.getCountry().equals("US")) {
            Hinta.setCellValueFactory(cellData -> new SimpleStringProperty(df.format(cellData.getValue().getHinta()) + " €"));
        } else{
            Hinta.setCellValueFactory(cellData -> new SimpleStringProperty(df.format(cellData.getValue().getHinta() * 1.13) + " $"));
        }
        Valmistaja.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getValmistajaId().getValmistajaNimi()));
        statusText.setText(tyontekija.getEtunimi()+  " " + tyontekija.getSukunimi());
        if(tyontekija.getStatus() == 1){
            adminbutton.setVisible(true);
            lisaaButton.setVisible(true);
            muokkaaButton.setVisible(true);

        }
        else{
            adminbutton.setVisible(false);
            lisaaButton.setVisible(false);
            muokkaaButton.setVisible(false);
        }
        setTuotelista(getTuoteLista());

    }

    /**
     * initData:lla tuomme MainApplication luokan loginview käyttöön.
     * @param mainApplication mainApplication luokka.
     */
    public void initData(MainApplication mainApplication){
        this.mainApplication = mainApplication;
    }

    /**
     * Avaa tuotteiden lisäys ikkunnan ja sen ohjaimen.
     */
    @FXML
    private void onAddButtonClick() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainController.class.getResource("edit-view.fxml"));
            loader.setResources(bundle);
            AnchorPane page = (AnchorPane) loader.load();
            Stage stage = new Stage();
            stage.setTitle(bundle.getString("AddTitle.text"));
            stage.initModality(Modality.WINDOW_MODAL);

            Scene scene = new Scene(page);
            stage.setScene(scene);

            stage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Avaa ohjauspaneelin ikkunan ja siihen liittyvän ohjaimen.
     */
    @FXML
    private void handleAdmin(){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainController.class.getResource("admin-view.fxml"));
            loader.setResources(bundle);
            AnchorPane page = (AnchorPane) loader.load();
            Stage stage = new Stage();
            stage.setTitle(bundle.getString("AdminTitle.text"));
            stage.initModality(Modality.WINDOW_MODAL);

            Scene scene = new Scene(page);
            stage.setScene(scene);

            stage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * Avaa valitun tuotteen lisäys ikkunan.
     */
    @FXML
    private void onLisaaButtonClick(){
        Tuote tuote = (Tuote) tuotelista.getSelectionModel().getSelectedItem();
        if(tuote != null){
            lisääTuotetta(tuote);
        }
    }

    /**
     * Avaa tuotekoodilla olevan tuotteen lisäys ikkunan.
     */
    @FXML
    private void  onLisaaKoodillaButtonAction(){
        int haku;
        try {
            haku = Integer.parseInt(tuotekoodiHaku.getText());
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query tuoteQuery = session.createQuery("From Data.Tuote a where a.tuoteKoodi = '" + haku + "'");
            List<Tuote> lista = tuoteQuery.list();
            if(lista.get(0) != null){
                lisääTuotetta(lista.get(0));
            }else{
                //alert
            }
        }catch (NumberFormatException e ){
            //alert
        }

    }

    /**
     * Avaa muokkaus ruudun valitusta tuotteesta.
     */
    @FXML
    private void onMuokkaaButtonAction(){
        Tuote tuote = (Tuote) tuotelista.getSelectionModel().getSelectedItem();
        if(tuote != null){
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainController.class.getResource("edit-view.fxml"));
                loader.setResources(bundle);
                AnchorPane page = (AnchorPane) loader.load();
                Stage stage = new Stage();
                stage.setTitle(bundle.getString("EditTitle.text"));
                stage.initModality(Modality.WINDOW_MODAL);

                Scene scene = new Scene(page);
                stage.setScene(scene);

                EditController controller = loader.getController();
                controller.initData(tuote);

                stage.showAndWait();
                setTuotelista(getTuoteLista());
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Sulkee sovelluksen.
     * @throws IOException Ongelman jos tämä funktio ei toimi oikein.
     */
    @FXML
    private void onCloseButton() throws IOException {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
        mainApplication.start(mainApplication.stage);
    }

    /**
     * Avaa historia ikkunan ja siihen liittyvän ohjaimen.
     */
    @FXML
    private void onHistoriaButtonAction(){

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(bundle);
            loader.setLocation(MainController.class.getResource("historia-view.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage stage = new Stage();
            stage.setTitle(bundle.getString("HistoryTitle.text"));
            stage.initModality(Modality.WINDOW_MODAL);

            Scene scene = new Scene(page);
            stage.setScene(scene);



            stage.show();

        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Päivittää tuotelistan ja tuotetaulun.
     */
    @FXML
    private void onPaivitaButtonClick(){
        setTuotelista(getTuoteLista());
    }

    /**
     * Hakee listasta sopivan tuotteen joka on sama tai lähellä hakukentässä olevaa tekstiä.
     *
     */
    @FXML
    private void onHakuButtonClick(){
        String haku = hakuKenttä.getText();
        setTuotelista(haeTuoteLista(haku));


    }

    /**
     * Hakee tuotelistan tietokannasta ja lisääsen listaan.
     * @return tuotelista
     */
    private List<Tuote> getTuoteLista() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query tuoteQuery = session.createQuery("From Data.Tuote");
        List<Tuote> lista = tuoteQuery.list();
        return(lista);

    }

    /**
     * Hakee listasta sopivan tuotteen joka on sama tai lähellä hakukentässä olevaa tekstiä.
     * @param haku hakukentässä oleva teksti.
     * @return listan jossa on vain hakua kohtaavat asiat.
     */
    private List<Tuote> haeTuoteLista(String haku){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query tuoteQuery = session.createQuery("From Data.Tuote a where a.tuoteNimi like '%" + haku +"%'");
        List<Tuote> lista = tuoteQuery.list();
        return lista;

    }

    /**
     * Asettaa Tauluun kaikki listassa olevat tuotteet.
     * @param lista listatuotteista.
     */
    private void setTuotelista(List<Tuote> lista){
        tuotelista.getItems().clear();
        tuotelista.getItems().addAll(lista);
    }

    /**
     * Tarkistaa onko jossakin tuotteessa listassa määrä vähemmän kuin tuotteen sallittu minimäärä.
     */
    @FXML
    private void onCheckButtonAction(){
        List<Tuote> lista = getTuoteLista();
        List<Tuote> lopussa = lista.stream().filter(tuote -> tuote.getVaroitusRaja() > tuote.getMaara()).collect(Collectors.toList());
        setTuotelista(lopussa);
    }

    /**
     * Avaa tuotteen määrän muokkaamis ikkunan.
     * @param tuote tuote luokka
     */
    public void lisääTuotetta(Tuote tuote) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainController.class.getResource("saldo-view.fxml"));
            loader.setResources(bundle);
            AnchorPane page = (AnchorPane) loader.load();
            Stage stage = new Stage();
            stage.setTitle(bundle.getString("AddTitle.text"));
            stage.initModality(Modality.WINDOW_MODAL);

            Scene scene = new Scene(page);
            stage.setScene(scene);

            Saldocontroller controller = loader.getController();

            controller.initData(tuote);

            stage.showAndWait();
            setTuotelista(getTuoteLista());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}