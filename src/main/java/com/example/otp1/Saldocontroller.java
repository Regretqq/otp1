package com.example.otp1;

import Data.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ResourceBundle;

/***
 * Tuotteiden määrän muokkaamiseen tarkoitettu näkymän kontrolleri
 * @author Felix Uimonen
 * @version 1.0
 *
 */
public class Saldocontroller {
    /***
     * Muokattava tuote
     */
    private Tuote tuote;
    /***
     * Ikkunan sulkemisnappi
     */
    @FXML
    private Button closeButton;
    /***
     * Tuotten  määrän kertova teksit
     */
    @FXML
    private Text maaraText;
    /***
     * Tuotteen nimen kertova teksti
     */
    @FXML
    private Text tuotenimi;
    /***
     * Tekstikenttä johon syötetään tuotteen määrän muutos
     */
    @FXML
    private TextField muutosKenttä;
    /***
     * Tallentaa syötetyt muutokset
     */
    @FXML
    private Button saveButton;
    /***
     * Työntekijän muutoksen liitttyvän komenttin syöttökenttä
     */
    @FXML
    private TextArea komentti;
    /***
     * Antaa työntekijän valita onko muutos positiivinen vai negatiivinen
     */
    @FXML
    private ChoiceBox suuntaLaatikko;
    /***
     * Lokalisoinnissa käytettävä resurssipaketti
     */
    private ResourceBundle bundle;
    /***
     * Muutoksen tekevä työntekijä
     */
    private Tyontekija tyontekija;
    /***
     * Muutoksen suuruus
     */
    private int muutos;


    /***
     * Hakee lokalisoinnissa tarvittavan resurssipaketin ja alustaa suuntalaatiokon
     */
    @FXML
    private void initialize(){
        bundle = Bundle.getBundle().getResourceBundle();
        suuntaLaatikko.getItems().addAll(new String[]{bundle.getString("sisään.text"),bundle.getString("ulos.text")});
        suuntaLaatikko.getSelectionModel().selectFirst();

    }

    /***
     * Sulkee ikkunan
     */
    @FXML
    private void onCloseButton(){
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }

    /***
     * Saa edellisestä näkymästä parametrinä tuotteen jonka tiedot se asettaa oikeisiin kenttiin
     * @param tuote tuote, jota muokataan näkymässä
     */
    void initData(Tuote tuote)
    {
        this.tuote = tuote;
        this.tyontekija = SingletonTyontekija.getInstance().getTyontekija();
        tuotenimi.setText(tuote.getTuoteNimi());
        maaraText.setText(Integer.toString(tuote.getMaara()));
    }

    /***
     * Hakee tekstikentistä tiedot ja tekee niiden pohjalta muutoksia tuoteeseen
     */
    @FXML
    private void onSaveButtonClick(){
        if (muutosKenttä.getText().length() <= 0){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(bundle.getString("muutosPuuttuuAlert.text"));
            alert.show();
        } else {
            muutos = Integer.parseInt(muutosKenttä.getText());

            if(suuntaLaatikko.getSelectionModel().getSelectedIndex() == 1){
                tuote.muutaMaaraa(-muutos);
            }else{
                tuote.muutaMaaraa(muutos);
            }
            if(tallennaMuutos()){
                if (muutosKenttä.getText().length() > 0) {
                    Stage stage = (Stage) saveButton.getScene().getWindow();
                    stage.close();
                }
            }
        }
    }

    /***
     * Tallentaa muokatun tuotteen tietokantaan
     * @return virheentarkistus tietokantaan tallentamisen onnistumisesta
     */
    private boolean tallennaMuutos(){
        try{
            HibernateUtil.getHibernateUtil().save(tuote);
            tallennaHistoriaan();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /***
     * Tallentaa tuotteen muutoksesta tiedon tietokantaan
     */
    private void tallennaHistoriaan(){
        Muutoshistoria muutoshistoria = new Muutoshistoria();
        muutoshistoria.setPaivaMaara(LocalDate.now(ZoneId.systemDefault()));
        muutoshistoria.setTuote(tuote);
        muutoshistoria.setMuutosMaara(muutos);
        muutoshistoria.setTyontekija(tyontekija);
        muutoshistoria.setInAndOut(suuntaLaatikko.getSelectionModel().getSelectedIndex());
        muutoshistoria.setKommentti(komentti.getText());
        HibernateUtil.getHibernateUtil().save(muutoshistoria);

    }


}
