package com.example.otp1;


import Data.Bundle;
import Data.HibernateUtil;
import Data.Valmistaja;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.ResourceBundle;
/***
 * Add-Valmistaja ikkunnan ohjain.
 * @author Joni Tahvanainen, Leo Lehtiö
 * @version 1.1
 *
 */

public class AddValmistajaView {
    /**
     * Sulkee ikkunan.
     */
    @FXML
    private Button closeButton;
    /**
     * Valmistaja tekstikenttä.
     */
    @FXML
    private TextField text;

    Alert alert = new Alert(Alert.AlertType.ERROR);

    /***
     * Valmistjan lisäys funktio.
     * Tarkistaa annetun valmistajan ja lisää sen hibernaten avulla tietokantaan jos tarkastukset menee läpi.
     * Jos tarkistuksista ei päästä läpi antaa funktio käyttäjälle huomautuksen miksi ei mennyt läpi.
     */
    @FXML
    private void handleAdd(){
        ResourceBundle bundle = Bundle.getBundle().getResourceBundle();

        Valmistaja save = new Valmistaja();
        if(text.getText().length() <= 0) {
            alert.setContentText(bundle.getString("valmistajanimiPuuttuuAlert.text"));
            alert.show();
        } else if (text.getText().length() > 30) {
            alert.setContentText("valmistajanimiLiianPitkäAlert.text");
            alert.show();
        } else {
            save.setValmistajaNimi(text.getText());
            HibernateUtil.getHibernateUtil().save(save);
            Stage stage = (Stage) closeButton.getScene().getWindow();
            stage.close();
        }
    }
}
