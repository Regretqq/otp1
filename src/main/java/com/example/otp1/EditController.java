package com.example.otp1;

import Data.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.controlsfx.control.spreadsheet.SpreadsheetCellEditor;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.ResourceBundle;

/***
 * Tuotteen muokkaus ja uuden lisäys ikkunan ohjain.
 * Voi lisätä uuden tuoteen main ikkunan kautta painamalla "lisää uusi tuote" joka luo tyhjän ikkunan jossa voi lisätä uuden
 * tuotteen annettuihin tekstilaatikoihin.
 *
 * Muokkaus onnistuu kun valitsee main ikkunasta haluamansa tuoteen ja painaa "muokkaa" nappia.
 * Josta pääsee tähän ikkunnan ja kaikki tiedot ovat valmiiksi täytetty ja niitä voi muokata haluamansa mukaisesti.
 * @author Joni Tahvanaine, Felix Uimonen
 * @version 1
 */
public class EditController {
    /**
     * Lista valmistajista.
     */
    private List<Valmistaja> valmistajat;
    /**
     * Lista ALV:eista.
     */
    private List<ALV> alvList;
    /**
     * Tuote luokka
     */
    private Tuote tuoteet;
    /**
     * Resurssipaketti.
     */
    private ResourceBundle bundle;
    /**
     * Sulkemis nappi.
     */
    @FXML
    private Button closeButton;
    /**
     * Tuoteen nimi
     */
    @FXML
    private TextField tuotenimi;
    /**
     * Tuoteiden määrä.
     */
    @FXML
    private TextField maara;
    /**
     * Varoitusraja tuotteelle.
     */
    @FXML
    private TextField varoitus;
    /**
     * Tuotteen hinta.
     */
    @FXML
    private TextField hinta;
    /**
     * Valmitajien valinta boxi.
     */
    @FXML
    private ComboBox valmistajacombobox;
    /**
     * Tuoteen alv.
     */
    @FXML
    private ChoiceBox alv;
    /**
     * Decimal formatisoija muuttuja.
     */
    private DecimalFormat df;


    /**
     * Valmistelee muokkaus/lisäys ikkunan käyttöä varten.
     * Etsii tarvittavan resurssipaketin lokalisointia varten.
     * Myös määrittää tuoteet muuttjalle Luokan.
     */
    @FXML
    void initialize(){
        if(tuoteet == null){
            tuoteet = new Tuote();
        }
        bundle = Bundle.getBundle().getResourceBundle();
        update();
    }

    /**
     * Edellisestä ikkunasta saadut parametrit.
     * @param tuote Tuote() luokka.
     */
    @FXML
    void initData(Tuote tuote){
        this.tuoteet = tuote;
        df = new DecimalFormat("0.00");
        tuotenimi.setText(tuote.getTuoteNimi());
        if(!bundle.getString("kieli.text").equals("English")) {
            hinta.setText(Double.toString(tuote.getHinta()));
        }else{
            hinta.setText((df.format((tuote.getHinta()*1.13))));
        }

        maara.setText(Integer.toString(tuote.getMaara()));
        varoitus.setText(Integer.toString(tuote.getVaroitusRaja()));
        valmistajacombobox.setValue(tuote.getValmistajaId().getValmistajaNimi());
        alv.setValue(tuote.getAlv().getAlvprosentti());

    }


    /**
     * Tuotteen lisäys/muokkauksen tallennus tietokantaan hibernatin avulla.
     * Tämä funktio tarkistaa kaikki syötetyt tiedot ja ilmoittaa käyttäjälle jos ei mene läpi.
     */
    @FXML
    private void handleSave(){

        if (tuotenimi.getText().length() > 30) {
            createAlert(bundle.getString("tuotenimiLiianPitkäAlert.text"));
        } else if (tuotenimi.getText().length() < 1) {
            createAlert(bundle.getString("tuotenimiPuuttuuAlert.text"));
        } else {
            this.tuoteet.setTuoteNimi(tuotenimi.getText());

            if (hinta.getText().length() < 1) {
                createAlert(bundle.getString("tuotehintaPuttuuAlert.text"));
            } else {
                try {
                    if(!bundle.getString("kieli.text").equals("English")){
                        this.tuoteet.setHinta(Double.parseDouble(hinta.getText()));
                    }else{

                        this.tuoteet.setHinta(Double.parseDouble(df.format(Double.parseDouble(hinta.getText())/1.13)));
                    }

                }catch (NumberFormatException e) {
                    createAlert(bundle.getString("HintaEiOleNumeroAlert.text"));
                }

                if (maara.getText().length() < 1) {
                    createAlert(bundle.getString("tuoteMääräPuuttuuAlert.text"));

                } else {
                    try {
                        this.tuoteet.setMaara(Integer.parseInt(maara.getText()));
                    }catch(NumberFormatException e){
                        createAlert(bundle.getString("MaaraEiOleNumeroAlert.text"));
                    }

                    if (varoitus.getText().length() < 1) {
                        createAlert(bundle.getString("tuoteVaroitusrajaPuutttuuAlert.text"));

                    } else {
                        try{
                            tuoteet.setVaroitusRaja(Integer.parseInt(varoitus.getText()));
                        } catch (NumberFormatException e) {
                            createAlert(bundle.getString("VaroitusEiOleNumeroAlert.text"));
                        }

                        this.tuoteet.setValmistajaId(valmistajat.get(valmistajacombobox.getSelectionModel().getSelectedIndex()));
                        this.tuoteet.setAlv(alvList.get(alv.getSelectionModel().getSelectedIndex()));
                        HibernateUtil.getHibernateUtil().save(tuoteet);

                        Stage stage = (Stage) closeButton.getScene().getWindow();
                        stage.close();
                    }
                }
            }
        }
    }

    /**
     * Päivittää valmistaja ja ALV listat, jos niihin on lisätty jotain.
     */
    private void update(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query alvquery = session.createQuery("From Data.ALV");

        alvList = alvquery.list();

        alv.getItems().clear();

        for(int i = 0; i< alvList.size(); i++){
            alv.getItems().add(alvList.get(i).getAlvprosentti());
        }

        Query valmistajaQuery = session.createQuery("from Data.Valmistaja");

        valmistajat = valmistajaQuery.list();

        valmistajacombobox.getItems().clear();

        for(int i = 0; i<valmistajat.size();i++) {
            valmistajacombobox.getItems().add(valmistajat.get(i).getValmistajaNimi());

        }

    }

    /**
     * Avaa valmistajan lisäys ruudun.
     */
    @FXML
    private void handlevalmistaja(){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(bundle);
            loader.setLocation(MainController.class.getResource("add-valmistaja-view.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage stage = new Stage();
            stage.setTitle(bundle.getString("lisääValmistaja.text"));
            stage.initModality(Modality.WINDOW_MODAL);

            Scene scene = new Scene(page, 250, 80);
            stage.setScene(scene);

            stage.showAndWait();
            update();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Avaa ALV lisäys ikkunan.
     */
    @FXML
    private void handleAlv(){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(bundle);
            loader.setLocation(MainController.class.getResource("add-Alv-view.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage stage = new Stage();
            stage.setTitle(bundle.getString("lisääAlv.text"));
            stage.initModality(Modality.WINDOW_MODAL);

            Scene scene = new Scene(page, 250, 80);
            stage.setScene(scene);

            stage.showAndWait();
            update();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Tuottaa ilmotuuksen käyttäjälle.
     * @param alertText varoitusteksti
     */
    public void createAlert(String alertText){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(null);
        alert.setContentText(alertText);
        alert.showAndWait();

    }

}
