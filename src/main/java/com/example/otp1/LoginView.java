package com.example.otp1;

import Data.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
/***
 * LoginView on kirjautumis ruudun ohjain. Jossa valitaan myös sovelluksen koko kieli.
 * Ja määritellään SingletonTyöntekijä luokkaan instance muuttuja, joka on henkilö joka kirjatuu sisään.
 *
 * @author Joni Tahvanainen
 * @version 1.2
 *
 */
public class LoginView {
    /**
     * Käyttäjätunnus.
     */
    private String username;
    /**
     * mainaplication luokka.
     */
    MainApplication mainApplication;
    /**
     * käyttäjä tekstikenttä.
     */
    @FXML
    private TextField user;
    /**
     * Kielen vaihto valintavalikko.
     */
    @FXML
    private ComboBox kieliboxi;
    /**
     * salasana kenttä.
     */
    @FXML
    private PasswordField pass;
    /**
     * Käyttäjä teksti.
     */
    @FXML
    private Text kayttaja;
    /**
     * Salasana teksti.
     */
    @FXML
    private Text salasana;
    /**
     * Kirjautumis nappi.
     */
    @FXML
    private Button sisaan;

    /**
     * Resurssipaketti.
     */
    ResourceBundle bundle;
    /**
     * Lista työntekijöistä.
     */
    private List<Tyontekija> työntekijälista;
    /**
     * Lista nimisti.
     */
    private List<String> name = new ArrayList<>();
    /**
     * Lista salasanoista.
     */
    private List<String> passes = new ArrayList<>();
    /**
     * Lista käyttäjän roolista.
     */
    private List<Integer> status = new ArrayList<>();

    /***
     * Valmistellaan loginview ikkunan käyttöä varten.
     */
    @FXML
    private void initialize(){
        bundle = Bundle.getBundle().getResourceBundle();
        kieliboxi.getItems().addAll("Suomi", "English");
        kieliboxi.getSelectionModel().selectFirst();
        haeTyötekijät();
        System.out.println(name);
        System.out.println(passes);
    }

    /**
     * initData:lla tuomme MainApplication luokan loginview käyttöön.
     * @param mainApplication mainApplication luokka.
     */
    public void initData(MainApplication mainApplication){
        this.mainApplication = mainApplication;
    }

    /***
     * Käyttöliittymästä valittu kieli tulee käyttöön.
     */
    @FXML
    private void handleLang(){
        if(kieliboxi.getSelectionModel().getSelectedItem().equals("Suomi")){
            Bundle.getBundle().setLocale("fi", "FI");

        }
        else{
            Bundle.getBundle().setLocale("en", "US");

        }
        bundle = Bundle.getBundle().getResourceBundle();
        kayttaja.setText(bundle.getString("Kayttajatunnus.text"));
        salasana.setText(bundle.getString("salasana.text"));
        sisaan.setText(bundle.getString("SisaanKirjautuminen.text"));

    }

    /***
     * handlelogin käsittelee kirjautmisen.
     * Tarkistetaan onko henkilö tietokannassa ja sitten onko henkilöllä oikea salasan joka on tietokannassa.
     * @throws IOException
     */
    @FXML
    private void handlelogin() throws IOException {
        username = user.getText();
        if(name.contains(username)) {
            String password = pass.getText();
            if (Objects.equals(passes.get(name.indexOf(username)), Encrypt.encrypt(password))){
                if (status.get(name.indexOf(username)) == 3){
                    createAlert(bundle.getString("Disabled.text"));

                } else {
                    Tyontekija tyontekija = työntekijälista.get(name.indexOf(username));
                    SingletonTyontekija.setIntance(tyontekija);
                    if(kieliboxi.getSelectionModel().getSelectedItem().equals("Suomi")){
                        Bundle.getBundle().setLocale("fi", "FI");

                    }
                    else{
                        Bundle.getBundle().setLocale("en", "US");

                    }
                    mainApplication.update();
                }
            }
            else{
                createAlert(bundle.getString("väärin.text"));
            }
        }
        else{
            createAlert(bundle.getString("väärin.text"));
        }
    }

    /***
     * Haetaan kaikki työntekijän tietokannasta ja heidän tiedot.
     * Tämän jälkeen kaikista haetuista tiedoista kootaan listoja.
     * salasanoille omansa, etunimille ja sukunimille omansa ja viimeiseksi heidän työ tilastaan.
     */

    private void haeTyötekijät(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query tuoteQuery = session.createQuery("From Data.Tyontekija");
        työntekijälista = tuoteQuery.list();
        for(int i = 0; i < työntekijälista.size(); i++){
            passes.add(työntekijälista.get(i).getPass());
            name.add(työntekijälista.get(i).getEtunimi() + " " + työntekijälista.get(i).getSukunimi());
            status.add(työntekijälista.get(i).getStatus());
        }
    }

    /***
     * Luo alertin.
     * @param alertText Virheilmoitus
     */
    private void createAlert(String alertText){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(null);
        alert.setContentText(alertText);
        alert.showAndWait();

    }

}
